# Define base image

FROM node:latest

# Specify/Create a working directory

WORKDIR /usr/src/app

# Copy Dependencies

COPY package*.json ./
RUN npm install

# Bundle our source code

COPY . .

# Expose docker ports

EXPOSE 8000

# Command that executes the images launces

CMD ["node","index.js"]